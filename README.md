### Ownership

Maintaining ownership and control over our data is crucial for our business adaptability, growth, and scalability. We must oversee it location, format, structure, and security. 
For 3rd party products and service offerings (e.g. SaaS, PaaS, or any cloud hosted solution), ensure that the access and data retention of the provider meets our business requirements.
Lacking direct data access may jeopardize our cost leverage, security, stability, and risk data loss.

### Conventions

Consistency in naming conventions across enterprise and business applications fosters learning, familiarity, and productivity while minimizing errors, confusion, and ambiguity.

#### Naming

 - Plural Table Names
   
    - As a benefit, it's easier to find the reference to the table name in application code because entities are singular. Example: `users`, `roles`, `stores`, `products`
    
- Lower Snake Casing

    - Utilizing snake case for table and field names ensures clarity, especially when dealing with acronyms or case exceptions.

    ```sql
    -- good examples:
    user_roles, store_products, customer_addresses, ipad_packages, asw_connections
    
    -- bad examples:
    iPadPackages, IpadPackages, IPadPackages, aswConnections, AswConnections, ASWConnections
    ```

- Specificity

  Avoid redundant naming by assuming fields belong to their respective tables. For instance, in a customers table, the name field refers to the customer's name. Additionally, when denormalizing relational properties, maintain consistency with a prefixed entity.

    ```sql
    -- good examples:
    customers.name,  orders.total
    
    -- bad examples:
    vendors.vendor_name, products.product_number
    ```
  - When referencing denormalized relational properties, use a consistent entity prefix
  
  ```sql
  products.vendor_name, products.vendor_code, orders.tax_rate, orders.tax_total
  ```

- Normalization 
  
  - From having a 1000+ column table to having EAVs (Entity Attribute Value, which is basically a key-value store), normalization is the process of deciding how and where to store data relationships. You can try to classify how normalized or de-normalized a table structure is[^8], but that's not important. We need to understand why a database was designed with 1000 columns for a table (eg. for read performance) or so abstract that you have to query down a chain of 500 key value stores (eg. for design flexibility). Communicating database design facilitates understanding and/or growth.

Common Field Names

| Field                          | Purpose                                                      |
| ------------------------------ | ------------------------------------------------------------ |
| `id`                           | Unsigned integer uniquely generated and maintained by the database server as a primary key. Be sure to use an integer large enough to store values for the lifetime of the application (eg. `bigint unsigned not null auto_increment`) |
| `name`                         | The short name of the entity                                 |
| `description`                  | Text that helps describe the entity beyond a self-explanatory name |
| `code`                         | The business/natural/domain key used in the real world. Eg. "Item Number," "Customer Number", "ASI Number" |
| `parent_id`                    | Used for foreign keys that reference the same table.  Eg. `category.parent_id` indicates that categories have a hierarchy. |
| `{evented}_at`                 | Datetimes at which something took place. Most commonly, `created_at` (not nullable),  `updated_at`, and `deleted_at` (nullable). |
| `{events}_from`, `{events}_to` | Indicates a lower and upper date range constraint            |


#### Random Notes / TODO:

**TIMESTAMP** has a range of '1970-01-01 00:00:01' UTC to '2038-01-19 03:14:07' UTC.

In most cases, all dates should be stored in UTC and localized by the application to match the user's timezone.  Storing dates in UTC enhances data consistency, accuracy, comparability, and avoids ambiguity, making it a best practice in many software applications, particularly those operating in distributed or international contexts.



